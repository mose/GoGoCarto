Contributing
============

Always create a branch for your specific purpose (example: "feature-share", or relative to particular issue: "issue-#456-bug-in-geocoder")

When you're done with your work, create a merge request and wait for the validation by the administrator.

Please comment as possible your work!

<?php

namespace App\Helper;

class SaasHelper
{
    private $ROOT_PROJECT_CODE = 'gogocarto_default';

    // GoGoCarto can be use as a SAAS. in this case, each subdomain map a projet
    // project1.gogo.carto, project2.gogo.carto etc...
    // by default, the projet code is "gogocarto_default"
    public function getCurrentProjectCode()
    {
        $dbName = $this->ROOT_PROJECT_CODE;
        $host = array_key_exists('HTTP_HOST', $_SERVER) ? $_SERVER['HTTP_HOST'] : null;

        if ($host) {
            $exploded = explode('.', $host);
            $subdomain = $exploded[0];
            if (count($exploded) >= 3 && !in_array($subdomain, ['dev', 'test', 'demo', 'carto', 'carto-dev', 'www'])) {
                $dbName = $subdomain;
            }
        }

        return $dbName;
    }

    public function isRootProject()
    {
        return $this->getCurrentProjectCode() == $this->ROOT_PROJECT_CODE;
    }

    // return the Url to the actual public folder (the web/ folder)
    public function getPublicFolderUrl()
    {
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            $url = $_SERVER['HTTP_ORIGIN'];
        } else {
            $url = (isset($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] : 'http').'://'.$_SERVER['HTTP_HOST'];
        }

        if (false !== strpos($url, 'localhost')) {
            $url = str_replace($_SERVER['SCRIPT_NAME'], '', $url);
        } // ugly fix to support localhost !

        return $url;
    }
}
